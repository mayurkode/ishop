import React from 'react';
import { Text, FlatList, View } from 'react-native';

import { useSelector, useDispatch } from 'react-redux';

import ProductItem from '../components/shop/ProductItem';

import * as cartActions from '../store/actions/cart';

// to add header icons 
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import CustomHeaderButton from '../components/UI/HeaderButton';

const ProductOverviewScreen = (props) => {

    const dispatch = useDispatch();

    const products = useSelector(state => state.products.availableProducts);

    const renderItemHandler = (itemData) => {
        return (
            <ProductItem
                image={itemData.item.image}
                title={itemData.item.title}
                price={itemData.item.price}
                onViewDetails={() => props.navigation.navigate('productDetails',
                    { productId: itemData.item.id, productTitle: itemData.item.title })}
                onAddToCart={() => {
                    dispatch(cartActions.addToCart(itemData.item));
                }}
            />
        )
    }
    return (
        <FlatList data={products}
            keyExtractor={(item) => item.id}
            renderItem={renderItemHandler} />
    );
}

ProductOverviewScreen.navigationOptions = navData => {
    return {
        headerTitle: 'All Products',
        headerLeft: <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
            <Item title="Menu" iconName='md-menu' onPress={ ()=> navData.navigation.toggleDrawer() }/>
        </HeaderButtons>,
        headerRight: <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
            <Item title="cart" iconName='md-cart' onPress= { ()=> navData.navigation.navigate('cart')}></Item>
        </HeaderButtons>
    }
}

export default ProductOverviewScreen;