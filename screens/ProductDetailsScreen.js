import React from 'react';
import { ScrollView, Text, StyleSheet, Image, Button, View } from 'react-native';

import { useSelector, useDispatch } from 'react-redux';
import * as CartActions from '../store/actions/cart';
import Colors from '../constants/Colors';

const ProductDetailsScreen = (props) => {

    const dispatch = useDispatch();

    const productId = props.navigation.getParam('productId');
    const selectedProduct = useSelector(state => state.products.availableProducts.find(prod => prod.id === productId));

    return (
        <ScrollView>
            <Image style={look.image} source={{ uri: selectedProduct.image }} />
            <View style={look.btn}>
                <Button color={Colors.primary} title="Add to Cart" onPress={
                    () => {
                        console.log('add to cart');
                        dispatch(CartActions.addToCart(selectedProduct))
                    }} />
            </View>
            <Text style={look.title}>{selectedProduct.title}</Text>
            <Text style={look.price}>${selectedProduct.price}</Text>
            <Text style={look.desc}>{selectedProduct.description}</Text>
        </ScrollView>
    )
};


ProductDetailsScreen.navigationOptions = navData => {
    return {
        headerTitle: navData.navigation.getParam('productTitle')
    }
}


const look = StyleSheet.create({
    image: {
        height: 300,
        width: '100%'
    },
    title: {
        fontFamily: 'open-sans-bold',
        fontSize: 24,
        color: Colors.primary,
        textAlign: "center",
        marginVertical: 10
    },
    price: {
        fontFamily: 'open-sans',
        fontSize: 20,
        textAlign: "center",
        marginVertical: 10
    },
    desc: {
        fontFamily: 'open-sans',
        textAlign: "center",
        marginVertical: 10,
        marginHorizontal: 20

    },
    btn: {
        marginVertical: 10,
        alignItems: 'center',
    }
})

export default ProductDetailsScreen;